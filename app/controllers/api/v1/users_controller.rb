class Api::V1::UsersController < ApplicationController
  before_action :remove_white_spaces, only: %i[search]
  before_action :set_users, only: %i[search]

  def index
    @users = User.all
  end

  def search
    render json: @users.pluck(:fullname)
  end

  private

  def remove_white_spaces
    params[:user][:fullname] = params[:user][:fullname].strip
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_users
    @users = MatchUsersService.new(user_params[:fullname]).perform
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(:fullname)
  end
end
