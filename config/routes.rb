Rails.application.routes.draw do
  namespace :api do
    scope module: :v1, defaults: { format: :json } do
      resources :users, only: [:index] do
        collection do
          get :search
        end
      end
    end
  end
end
