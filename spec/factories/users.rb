FactoryBot.define do
  factory :user do
    fullname { Faker::Name.name }
  end
end
