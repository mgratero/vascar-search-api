require 'rails_helper'

describe MatchUsersService do
  context 'Matching rules' do
    let!(:user_jones) { FactoryBot.create(:user, fullname: 'Jones') }
    let!(:user_jonas) { FactoryBot.create(:user, fullname: 'Jonas') }
    let!(:user_jonathan) { FactoryBot.create(:user, fullname: 'Jonathan') }
    let!(:user_li) { FactoryBot.create(:user, fullname: 'Li') }
    it 'Match with first 3 characters' do
      users = MatchUsersService.new('jon').perform
      expect(users.size).to eql(3)
    end
    it 'Not a Match with first 2 characters' do
      users = MatchUsersService.new('jo').perform
      expect(users.size).to eql(0)
    end
    it 'Match with only 2 characters' do
      users = MatchUsersService.new('li').perform
      expect(users.size).to eql(1)
    end
    it 'Not Match beyond 3 characters' do
      users = MatchUsersService.new('Jonasi').perform
      expect(users.size).to eql(0)
    end
  end
end
